

	<header>

		<div class="container container-twelve">

			<div class="columns four alpha">
				<h1><a href="<?php echo home_url(); ?>"><?php bloginfo( 'name' ); ?></a></h1>
				<?php bloginfo( 'description' ); ?>
			</div>

			<div class="columns six omega offset-by-two">

				<?php get_search_form(); ?>
			</div>

		</div>

	</header>
